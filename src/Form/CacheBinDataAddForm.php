<?php

namespace Drupal\cache_ui\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CacheBinDataAddForm extends FormBase {

  public function getFormId() {
    return 'cache_ui_cache_bin_data_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $bin = NULL, $cid = NULL) {
    $form['bin'] = [
      '#type' => 'textfield',
      '#attributes' => [
        'readonly' => TRUE,
        'disabled' => TRUE,
      ],
      '#title' => $this->t('Cache Bin'),
      '#default_value' => $bin,
    ];


    $form['cid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CID'),
      '#description' => $this->t('Cache key.'),
    ];

    $form['tags'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tags'),
      '#description' => $this->t('Enter tags space-separated.'),
      '#rows' => 3,
    ];

    $form['data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data'),
      '#description' => $this->t('Serialized values will be flagged as usual.'),
      '#rows' => 6,
      '#prefix' => '<div id="edit-export-wrapper">',
      '#suffix' => '</div>',
      '#default_value' => '',
    ];

    $form['expire'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expire'),
      '#default_value' => '-1',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\Core\Cache\CacheBackendInterface $cache_bin */
    $cache_bin = \Drupal::service($form_state->getValue('bin'));

    $data = $form_state->getValue('data');
    if ($unserialized = @unserialize($data)) {
      $data = $unserialized;
    }

    $cache_bin->set(
      $form_state->getValue('cid'),
      $data,
      $form_state->getValue('expire'),
      explode(' ', $form_state->getValue('tags')) ?? [],
    );

    $form_state->setRedirect('cache_ui.cache_bin_keys', ['bin' => $form_state->getValue('bin')]);
  }
}
